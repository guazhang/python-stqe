setup: True

/create_loopdev:
    description: Creates loopdev
    test: create_loopdev.py
    loopdev_size: 1  # Size in MB
    requires_cleanup: targetcli/setup/loopdev_delete

/loopdev_delete:
    description: Deletes loopdev.
    test: delete_loopdev.py

/create_savefile:
    description: Saves configuration as saveconfig file
    test: create_saveconfig.py

/create_block:
    description: Creates block backstore object
    test: create_backstore.py
    backstore_type: block
    backstore_name: block
    loopdev: True
    requires_cleanup: targetcli/setup/delete_block

/create_fileio:
    description: Creates fileio backstore object
    test: create_backstore.py
    backstore_type: fileio
    backstore_name: fileio
    file_or_dev: fileio_file
    size: 1M
    requires_cleanup: targetcli/setup/delete_fileio

/create_fileio_1g:
    description: Creates fileio backstore object
    test: create_backstore.py
    backstore_type: fileio
    backstore_name: fileio
    file_or_dev: fileio_file
    size: 1G
    requires_cleanup: targetcli/setup/delete_fileio

/create_ramdisk:
    description: Creates ramdisk backstore object
    test: create_backstore.py
    backstore_type: ramdisk
    backstore_name: ramdisk
    size: 1M
    requires_cleanup: targetcli/setup/delete_ramdisk

/delete_block:
    description: Deletes block backstore object
    test: delete_backstore.py
    backstore: block

/delete_fileio:
    description: Deletes fileio backstore object
    test: delete_backstore.py
    backstore: fileio

/delete_ramdisk:
    description: Deletes ramdisk backstore object
    test: delete_backstore.py
    backstore: ramdisk

/create_loopback:
    description: Creates loopback object based on fileio backstore
    test: create_loopback.py
    lun: 255
    requires_cleanup: targetcli/setup/delete_loopback

/delete_loopback:
    description: Deletes loopback object
    test: delete_loopback.py

/install_tcmu_runner:
    description: Installs TCMU runner
    test: install_tcmu_runner.py
    tcmu_path: /root/tcmu_runner
    tcmu_repo_link: https://github.com/open-iscsi/tcmu-runner.git

/create_qcow:
    description: Creates qcow2 image
    test: create_qcow.py
    filename: qcow_test
    img_size: 1G
    fmt: qcow2
    preallocation: metadata
    lazy_refcounts: "on"
    requires_cleanup: targetcli/setup/delete_qcow

/create_qcow_backstore:
    description: Creates user:qcow backstore
    test: create_backstore.py
    backstore_type: user:qcow
    backstore_name: qcow_backstore
    size: 1G
    requires_cleanup: targetcli/setup/delete_qcow_backstore

/delete_qcow:
    description: Deletes qcow image
    test: delete_qcow.py

/delete_qcow_backstore:
    description: Deletes user:qcow backstore
    test: delete_backstore.py
    backstore: user:qcow

/create_qcow_target:
    description: Creates iscsi target with a user:qcow storage object
    test: create_qcow_target.py
    lun: 255
    requires_cleanup: targetcli/setup/delete_iscsi_qcow_target

/delete_iscsi_target:
    description: Deletes iscsi target
    test: delete_iscsi_target.py

/delete_iscsi_qcow_target:
    description: Deletes iscsi qcow target
    test: delete_iscsi_target.py

/create_fileio_target:
    description: Creates iscsi target with a fileio storage object
    test: create_fileio_target.py
    requires_cleanup: targetcli/setup/delete_iscsi_target

/disable_chap:
    description: Disables CHAP and removes password and userid from iscsid config.
    test: disable_chap.py

/configure_chap:
    description: Configures 1-way CHAP for iSCSI tests
    test: configure_chap.py
    userid: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-+@_=:/[],~'
    password: '.-+@_=:/[],~9876543210ZYXWVUTSRQPONMLKJIHGFEDCBAzyxwvutsrqponmlkjihgfedcba'
    requires_cleanup: targetcli/setup/disable_chap

/configure_chap_2ways:
    description: Configures 2-way CHAP for iSCSI tests
    test: configure_chap.py
    userid: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-+@_=:/[],~'
    password: '.-+@_=:/[],~9876543210ZYXWVUTSRQPONMLKJIHGFEDCBAzyxwvutsrqponmlkjihgfedcba'
    mutual_userid: '0123456789.-+@_=:/[],~abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
    mutual_password: '.-+@_=:/[],~abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789'
    requires_cleanup: targetcli/setup/disable_chap

/configure_chap_2ways_short:
    description: Configures 2-way CHAP for iSCSI tests with a single character as userid and password
    test: configure_chap.py
    userid: 'a'
    password: '0'
    mutual_userid: 'A'
    mutual_password: '1'
    requires_cleanup: targetcli/setup/disable_chap

/configure_chap_2ways_long:
    description: Configures 2-way CHAP for iSCSI tests with 254 characters as userid and password
    test: configure_chap.py
    userid: 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-+@_=:/[],~abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-+@_=:/[],~abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-+@_=:/[],~abcdefghijklmnopqrs.-+@_=:/[],~'
    password: '.-+@_=:/[],~abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-+@_=:/[],~abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-+@_=:/[],~abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-+@_=:/[],~abcdefghijklmnopqrs'
    mutual_userid: '0123456789.-+@_=:/[],~abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-+@_=:/[],~abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-+@_=:/[],~abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-+@_=:/[],~abcdefghijklmnopqrs'
    mutual_password: '.-+@_=:/[],~abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.-+@_=:/[],~abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-+@_=:/[],~abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789.-+@_=:/[],~abcdefghijklmnopqrs0123456789'
    requires_cleanup: targetcli/setup/disable_chap

/configure_chap_random:
    description: Configures 1-way CHAP for iSCSI tests with random credentials
    test: configure_chap.py
    random: True
    requires_cleanup: targetcli/setup/disable_chap

/configure_chap_2ways_random:
    description: Configures 2-way CHAP for iSCSI tests with random credentials
    test: configure_chap.py
    chap_ways: 2
    random: True
    requires_cleanup: targetcli/setup/disable_chap

/stop_multipath:
    descirption: Stops multipathd and multipathd.socket services
    test: stop_multipath.py
    requires_cleanup: targetcli/setup/start_multipath

/start_multipath:
    description: Starts multipathd and multipathd.socket services
    test: start_multipath.py
